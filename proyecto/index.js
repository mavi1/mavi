var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var usuario=require('./modelos/usuario');
var validator = require("email-validator");
const bcrypt = require('bcrypt');
 
const mongoose = require('mongoose');

 //modelo movimientos
 app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");  
    next();
  });
  mongoose.connect("mongodb://admin1:admin1@ds213199.mlab.com:13199/usuarios", { connectTimeoutMS: 5000 }, (err, res) => {
    if (err) {
    throw err
    } else {
    console.log(`Base de datos Online`);
    }
  });
  app.use(bodyParser.urlencoded({ extended: false }))
  
  // parse application/json
  app.use(bodyParser.json())


app.get('/', function (req, res) {
  res.send('Hello World')
})
 
app.get('/datos', function (req, res) {
    res.send('Hello World')
  })

  app.post('/usua',function(req,res){

    if(validator.validate(req.body.correo)){

    }
    else{
        return res.status (400).json({
            "mensaje":"formato de correo incorrecto"
        })
    }
   

    let usua = new usuario({
    correo:req.body.correo,
    clave:bcrypt.hashSync(req.body.clave, 10)
    
    })
    usua.save((err,usuariodb)=>{
      if (err){
      return  res.status(200).json({
          "error":"error inesperado"
        })
      }
      //////
     return res.status(200).json({
        movimiento:usuariodb
      })
    })
  
  })

  app.get('/buscar01/:correo/:clave', function (req, res) { 
    var correo1=req.params.correo;
    var clave1=req.params.clave;
  
   console.info(clave1);
  
  
   usuario.findOne({correo:correo1}, (err,usuariodb)=>{
    if (err){
    return  res.status(200).json({
        "error":"error inesperado"
      })
    }

    if(usuariodb==null){
        return res.status(400).json({
          correo: "no existe"
        })
      }


    if(bcrypt.compareSync(clave1, usuariodb.clave)){
        return res.status(200).json({
            correo:usuariodb
          })
    }else{
        return res.status(400).json({
          correo: "clave incorrecta"
        })   }
  })
  
  });
  
   

app.listen(3000)