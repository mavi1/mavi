const mongoose = require('mongoose');
let Schema=mongoose.Schema;



// es un objeto del tipo persona
let persona = new Schema({
    nombre:{type:String},
    apellido:{type:String},
    dni:{type:String}
})

//forma de exponer el objeto persona
module.exports=mongoose.model('persona',persona);
